import pandas as pd
from proceset import Proceset
from bot_keys import API_KEY, WEBHOOK_ID, BASE_URL

model = Proceset(BASE_URL, API_KEY)
print('GUID: ',  model.get_guid())
# test = pd.DataFrame(columns = ['id','name'], data = [['1','41'],['3','new']])
# print(model.execute_script_with_data(404, test))

test = pd.DataFrame(columns = ['f_1','f_2', 'f_3'], data = [['1','41','y'],['3','new','n']])
print(model.webhook_upload(WEBHOOK_ID, test))
